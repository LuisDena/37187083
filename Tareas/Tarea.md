
## Poema: "Reconocer lo infinito"

### Juan Luis Dena Juárez

>***Agoniza a la primera palabra.
Se lanza con el juicio,
apuesta escondida sin la bestia.***

>***Ya no formula.
El punto en el agua crece
con círculos boreales, sin materia.
Mil y un mares atestiguan el suicidio:
el hilo retorna
y forcejea con voces iracundas***

>***Con el final,
sale del río, vuela, imagina.
Un nuevo Libro se inscribe y se decanta***

>>***-Salvador Lira***

![Imágen Referencia](https://www.elsotano.com/imagenes_grandes/9786078/978607802835.JPG)